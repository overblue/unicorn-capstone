import os
import sys
import time
import struct
import binascii
import tempfile
import subprocess

# parsing ELF files (programmers are ELF files)
from elftools.elf.elffile import ELFFile
from elftools.elf.constants import P_FLAGS
from elftools.elf.descriptions import describe_p_flags

# emulating the programmer
from unicorn import Uc, UC_ARCH_ARM, UC_MODE_THUMB, UC_ARCH_ARM64, UC_MODE_ARM
from unicorn.arm_const import *
from unicorn.arm64_const import *

# disassembly of programmer instructions
from capstone import Cs, CS_ARCH_ARM, CS_MODE_THUMB, CS_ARCH_ARM64, CS_MODE_ARM


# get unicorn register name from a disassebled register name/string
def uc_get_reg(uc_arch, reg_name):
    reg_name = reg_name.upper().strip()
    if UC_ARCH_ARM == uc_arch:
        import unicorn.arm_const
        reg = 'UC_ARM_REG_' + reg_name
        module = unicorn.arm_const
    elif UC_ARCH_ARM64 == uc_arch:
        import unicorn.arm64_const
        reg = 'UC_ARM64_REG_' + reg_name
        module = unicorn.arm64_const
    else:
        raise NotImplementedError
    reg = getattr(module, reg, None)
    assert reg is not None
    return reg


# align size up to the next page boundary
def align_page_size(size):
    if size & 0xfff != 0:
        size = size & ~0xfff
        size = size + 0x1000
    return size

# parse programmer
def parse_programmer(path):
    # open the programmer binary
    with open(path, 'rb') as handle:

        # parse ELF header
        elf_file = ELFFile(handle)
        elf_header = elf_file.header

        # use the ELF header to determine capstone/unicorn architecture
        if 'EM_AARCH64' == elf_header['e_machine']:
            uc_arch = UC_ARCH_ARM64
            cs_arch = CS_ARCH_ARM64
            uc_mode = UC_MODE_ARM
            cs_mode = CS_MODE_ARM
        else:
            uc_arch = UC_ARCH_ARM
            cs_arch = CS_ARCH_ARM
            uc_mode = UC_MODE_ARM
            cs_mode = CS_MODE_ARM

        # use the ELF header to determine which pc/sp registers to use
        if 'EM_AARCH64' == elf_header['e_machine']:
            uc_reg_pc = UC_ARM64_REG_PC
            uc_reg_sp = UC_ARM64_REG_SP
        else:
            uc_reg_pc = UC_ARM_REG_PC
            uc_reg_sp = UC_ARM_REG_SP

        # create a capstone engine for disassembly
        md = Cs(cs_arch, cs_mode)

        # create a unicorn engine for emulation
        mu = Uc(uc_arch, uc_mode)

        # copy just the executable sections into emulator
        # we can get these from the ELF file we parsed
        for segment in elf_file.iter_segments():

            # ignore all segments that are not marked for LOADing
            if 'PT_LOAD' != segment['p_type']:
                continue

            # only interested in executable sections in the programmer
            if ( P_FLAGS.PF_X & segment['p_flags']) == 0:
                continue

            # read the code from the ELF file
            offset = segment['p_offset']
            size = segment['p_filesz']
            handle.seek(offset)
            data = handle.read(size)
            assert len(data) == size

            # get the location and size to map it to in the emulator
            vaddr = segment['p_vaddr']
            vsize = segment['p_memsz']
            vsize = align_page_size(vsize)

            # map executable code into emulator
            try:
                mu.mem_map(vaddr, vsize)
                mu.mem_write(vaddr, data)
            except:
                # some memory regions overlap
                # just ignore it
                pass

        # use the ELF header to find the programmer's entry point
        entry = elf_header['e_entry']
        print("entry point @ " + hex(entry))

        # set the pc to start executing at the programmer's entry point
        mu.reg_write(uc_reg_pc, entry)

        # during execution in the emulator, I want to get the stack, vbar and EL
        stack = None
        vbar = None
        el = None
        while not stack or not vbar or not el:
            # pc from emulator
            pc = mu.reg_read(uc_reg_pc)

            # opcode (instruction bytes) from emulator
            code = mu.mem_read(pc, 4)

            # disassemble with capstone
            ins = list(md.disasm(code, pc))[0]

            # what instruction is this??
            print("0x%x:\t%s\t%s" %(ins.address, ins.mnemonic, ins.op_str))

            # execute this instruction (and just this ONE instruction)
            mu.emu_start(mu.reg_read(uc_reg_pc), 0, count=1)

            # all registers start as ZERO
            # when they change we know they have been set

            # check for stack set
            if 0 != mu.reg_read(uc_reg_sp):
                stack = mu.reg_read(uc_reg_sp)
                print("stack @ " + hex(stack))

            # check for set vbar_el instruction arm64
            if ins.op_str.startswith('vbar_el'):
                reg = ins.op_str.split(',')[1]
                uc_reg = uc_get_reg(uc_arch, reg)
                vbar = mu.reg_read(uc_reg)
                el = int(ins.op_str[len('vbar_el'): len('vbar_el') + 1], 10)
                print("vbar_el{} @ ".format(el) + hex(vbar))

            # check for mcr p15 (vbar) instruction on arm
            if ins.mnemonic == 'mcr' and ins.op_str.startswith('p15'):
                reg = ins.op_str.split(',')[2]
                uc_reg = uc_get_reg(uc_arch, reg)
                vbar = mu.reg_read(uc_reg)
                # FIXME: only arm32 I know is nokia 6. Assume EL3
                el = 3
                print("vbar_el{} @ ".format(el) + hex(vbar))

        # stack is a RW segment WITHIN the programmer
        # so, search the ELF for the virtual address matching the stack
        # that segment's size will be the size of the stack
        stack_size = None
        for segment in elf_file.iter_segments():
            vaddr = segment['p_vaddr']
            vsize = segment['p_memsz']
            vaddr_min = vaddr
            vaddr_max = vaddr + vsize
            if vaddr + vsize == stack:
                stack_size = vsize
                break
            if stack > vaddr_min and stack < vaddr_max:
                stack_size = stack - vaddr
        print("stack size is " + hex(stack_size))

        # oneplus programmers have RWX segments
        # they are VERY useful, lets see if we can find any
        rwx = 0
        rwx_size = 0
        for segment in elf_file.iter_segments():
            # load segments that are RWX
            if 'PT_LOAD' != segment['p_type']:
                continue
            if ( P_FLAGS.PF_R & segment['p_flags']) == 0:
                continue
            if ( P_FLAGS.PF_W & segment['p_flags']) == 0:
                continue
            if ( P_FLAGS.PF_X & segment['p_flags']) == 0:
                continue

            # log address and size
            vaddr = segment['p_vaddr']
            vsize = segment['p_memsz']
            vsize = align_page_size(vsize)
            print("RWX page @ " + hex(vaddr) + ' [{} bytes]'.format(hex(vsize)))

def main():
    path = sys.argv[1]
    parse_programmer(path)

if __name__ == '__main__':
    main()
